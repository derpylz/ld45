"use strict";

var music = {
  tracks: {
    theme: new Audio('audio/theme.mp3')
  },
  maxVol: .5,
  currentTrack: undefined,
  play: function(track, fadeIn = false) {
    this.currentTrack = track;
    this.currentTrack.addEventListener('ended', function () {
      this.currentTime = 0;
      this.play();
    }, false);
    if (fadeIn) {
      this.currentTrack.volume = 0;
      this.currentTrack.play();
      this.fadeIn = setInterval( function () {
        fader(music.currentTrack);
      }, 100);
    } else {
      this.currentTrack.play();
    }
  },
  pause: function(fadeOut = false) {
    if (fadeOut) {
      this.fadeOut = setInterval( function () {
        fader(music.currentTrack, 10, false);
      }, 100);
    } else {
      this.currentTrack.pause();
    }
  }
}

var sound = {
  sounds: {
    pop: 'audio/pop.mp3',
    snake: 'audio/snake.mp3',
    wolf: 'audio/wolf.mp3',
    interact: 'audio/interact.mp3',
  },
  maxVol: .7,
  playing: [],
  play: function(soundSrc, vary) {
    var sfx = new Audio(soundSrc);
    sfx.currentTime = 0;
    sfx.volume = this.maxVol;
    if (vary) {
      sfx.playbackRate = randInt(8, 13) / 10;
    } else {
      sfx.playbackRate = 1;
    }
    sfx.addEventListener('ended', function () {
      for (var idx = 0; idx < sound.playing.length; idx++) {
        if (sound.playing[idx].ended) {
          sound.playing.splice(idx, 1);
        }
      }
    });
    sfx.play();
    this.playing.push(sfx);
  }
}

/**
 * Fades music in or out
 * @param {*} track 
 * @param {number} steps 
 * @param {boolean} In 
 */
function fader (track, steps = 10, In = true) {
  var step = music.maxVol / steps;
  if (In) {
    if (track.volume < 1) {
      var nxtVol = track.volume + step;
      if (nxtVol >= music.maxVol) {
        track.volume = music.maxVol;
        clearInterval(music.fadeIn);
      } else {
        track.volume = nxtVol;
      }
    }
  } else {
    if (track.volume > 0) {
      var nxtVol = track.volume - step;
      if (nxtVol <= 0) {
        track.volume = 0;
        clearInterval(music.fadeOut);
        track.pause();
      } else {
        track.volume = nxtVol;
      }
    }
  }
}