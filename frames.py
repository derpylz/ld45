from glob import glob
from scipy.misc import imresize
from imageio import imread, imwrite
import numpy as np

path = 'sprites/frames/'
image_paths = glob(path + 'item_flower_*.png')

print(len(image_paths))
frames = None

for img_path in image_paths:
    img = imread(img_path)
    if frames is None:
        frames = img
        print(img.shape)
    else:
        frames = np.concatenate((frames, img), axis=1)

print(img.shape)
print(frames.shape)

# factor = 1 # rescale factor
# frames = imresize(frames, factor*50, interp='nearest')

imwrite('sprites/item_flower.png', frames)