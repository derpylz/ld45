"use strict";

var gameStarted = false;

/**
 * Random integer between two numbers
 * @param {number} min
 * @param {number} max
 * @return {number}
 */
function randInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

/**
 * true every n frames
 * @param {number} n
 * @return {boolean}
 */
function everyInterval(n, gA) {
  if ((gA.frameNo / n) % 1 == 0) {
    return true;
  }
  return false;
}

/**
 * Vector object
 * @param {number} x
 * @param {number} y
 */
class Vector {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.length = Math.sqrt(this.x * this.x + this.y * this.y);
  }
  /**
   * Normalizes a vector
   */
  normalize() {
    this.x = this.x / this.length;
    this.y = this.y / this.length;
  }
  /**
   * Adds two vector objects together
   * @param {Vector} other
   */
  add(other) {
    var resultX = this.x + other.x;
    var resultY = this.y + other.y;
    return new Vector(resultX, resultY);
  }
}

function drawSpriteLoop(gA, img, spr, x, y) {
  gA.context.drawImage(
    img,
    (gA.frameNo % spr.anim) * spr.sh,
    0,
    spr.sh,
    spr.sh,
    x + spr.xoff,
    y + spr.yoff,
    spr.th - 10 + y / 20,
    spr.th - 10 + y / 20
  );
}

function drawSpriteOnce(gA, img, spr, x, y, counter) {
  if (counter > spr.anim) {
    counter = spr.anim;
  } else {
    counter += 1;
  }
  gA.context.drawImage(
    img,
    (counter - 1 % spr.anim) * spr.sh,
    0,
    spr.sh,
    spr.sh,
    x + spr.xoff,
    y + spr.yoff,
    spr.th - 10 + y / 20,
    spr.th - 10 + y / 20
  );
  return counter;
}

var sprites = {
  cursor: { name: 'cursor', url: 'sprites/cursor.png', sh: 20, th: 20, xoff: -10, yoff: -10, anim: 1 },
  cursor_clicked: { name: 'cursor_clicked', url: 'sprites/cursor_click.png', sh: 20, th: 20, xoff: -10, yoff: -10, anim: 12 },
  player_idle: { name: 'player_idle', url: 'sprites/player_idle.png', sh: 100, th: 50, xoff: -25, yoff: -25, anim: 28 },
  player_walking: { name: 'player_walking', url: 'sprites/player_walking.png', sh: 100, th: 50, xoff: -25, yoff: -25, anim: 32 },
  player_interact_item: { name: 'player_interact_item', url: 'sprites/player_interact_item.png', sh: 100, th: 50, xoff: -25, yoff: -25, anim: 68 },
  player_birth: { name: 'player_birth', url: 'sprites/player_birth.png', sh: 100, th: 50, xoff: -25, yoff: -25, anim: 26 },
  stick_hidden: { name: 'stick_hidden', url: 'sprites/stick_hidden.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 1 },
  stick_depleted: { name: 'stick_depleted', url: 'sprites/stick_depleted.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 22 },
  stone_hidden: { name: 'stone_hidden', url: 'sprites/stone_hidden.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 1 },
  stone_depleted: { name: 'stone_depleted', url: 'sprites/stone_depleted.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 22 },
  hole_hidden: { name: 'hole_hidden', url: 'sprites/hole_hidden.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 1 },
  hole_depleted: { name: 'hole_depleted', url: 'sprites/hole_depleted.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 22 },
  hole_trapped: { name: 'hole_trapped', url: 'sprites/hole_trapped.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 22 },
  hole_overcome: { name: 'hole_overcome', url: 'sprites/hole_overcome.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 13 },
  girl_hidden: { name: 'girl_hidden', url: 'sprites/girl_hidden.png', sh: 100, th: 50, xoff: -25, yoff: -25, anim: 28 },
  girl_depleted: { name: 'girl_depleted', url: 'sprites/girl_depleted.png', sh: 100, th: 50, xoff: -25, yoff: -25, anim: 24 },
  girl_trapped: { name: 'girl_trapped', url: 'sprites/girl_trapped.png', sh: 100, th: 50, xoff: -25, yoff: -25, anim: 6 },
  girl_overcome: { name: 'girl_overcome', url: 'sprites/girl_overcome.png', sh: 100, th: 50, xoff: -25, yoff: -25, anim: 70 },
  wolf_hidden: { name: 'wolf_hidden', url: 'sprites/wolf_hidden.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 100 },
  wolf_depleted: { name: 'wolf_depleted', url: 'sprites/wolf_depleted.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 22 },
  snake_hidden: { name: 'snake_hidden', url: 'sprites/snake_hidden.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 100 },
  snake_depleted: { name: 'snake_depleted', url: 'sprites/stone_depleted.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 22 },
  item_stick: { name: 'item_stick', url: 'sprites/item_stick.png', sh: 100, th: 50, xoff: -25, yoff: -25, anim: 15 },
  item_paper: { name: 'item_paper', url: 'sprites/item_paper.png', sh: 100, th: 50, xoff: -25, yoff: -25, anim: 15 },
  item_stone: { name: 'item_stone', url: 'sprites/item_stone.png', sh: 100, th: 50, xoff: -25, yoff: -25, anim: 15 },
  item_coin: { name: 'item_coin', url: 'sprites/item_coin.png', sh: 100, th: 50, xoff: -25, yoff: -25, anim: 15 },
  item_flower: { name: 'item_flower', url: 'sprites/item_flower.png', sh: 100, th: 50, xoff: -25, yoff: -25, anim: 15 },
  enemy_wolf_idle: { name: 'enemy_wolf_idle', url: 'sprites/enemy_wolf_run.png', sh: 100, th: 60, xoff: -30, yoff: -30, anim: 1 },
  enemy_wolf_run: { name: 'enemy_wolf_run', url: 'sprites/enemy_wolf_run.png', sh: 100, th: 60, xoff: -30, yoff: -30, anim: 27 },
  enemy_wolf_attack: { name: 'enemy_wolf_attack', url: 'sprites/enemy_wolf_attack.png', sh: 100, th: 60, xoff: -30, yoff: -30, anim: 20 },
  enemy_wolf_dead: { name: 'enemy_wolf_dead', url: 'sprites/enemy_wolf_dead.png', sh: 100, th: 60, xoff: -30, yoff: -30, anim: 24 },
  enemy_snake_idle: { name: 'enemy_snake_idle', url: 'sprites/enemy_snake_run.png', sh: 100, th: 60, xoff: -30, yoff: -30, anim: 1 },
  enemy_snake_run: { name: 'enemy_snake_run', url: 'sprites/enemy_snake_run.png', sh: 100, th: 60, xoff: -30, yoff: -30, anim: 27 },
  enemy_snake_attack: { name: 'enemy_snake_attack', url: 'sprites/enemy_snake_attack.png', sh: 100, th: 60, xoff: -30, yoff: -30, anim: 20 },
  enemy_snake_dead: { name: 'enemy_snake_dead', url: 'sprites/enemy_snake_dead.png', sh: 100, th: 60, xoff: -30, yoff: -30, anim: 26 },
  stick2paper_hidden: { name: 'trader_stick_paper', url: 'sprites/trader_stick_paper.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 38 },
  stick2paper_depleted: { name: 'trader_stick_paper_depleted', url: 'sprites/trader_stick_paper_depleted.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 22 },
  stone2coin_hidden: { name: 'trader_stone_coin', url: 'sprites/trader_stone_coin.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 38 },
  stone2coin_depleted: { name: 'trader_stone_coin_depleted', url: 'sprites/trader_stone_coin_depleted.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 22 },
  coin2flower_hidden: { name: 'trader_coin_flower', url: 'sprites/trader_coin_flower.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 38 },
  coin2flower_depleted: { name: 'trader_coin_flower_depleted', url: 'sprites/trader_coin_flower_depleted.png', sh: 100, th: 75, xoff: -37.5, yoff: -37.5, anim: 22 },
}

class Player {
  constructor(gA) {
    this.gA = gA;
    this.x = gA.clickedPos.x;
    this.y = gA.clickedPos.y;
    this.state = "pre";
    this.sprIdle = sprites.player_idle;
    this.imgIdle = new Image();
    this.imgIdle.src = this.sprIdle.url + "?" + new Date().getTime();
    this.sprWalk = sprites.player_walking;
    this.imgWalk = new Image();
    this.imgWalk.src = this.sprWalk.url + "?" + new Date().getTime();
    this.sprInteractItem = sprites.player_interact_item;
    this.imgInteractItem = new Image();
    this.imgInteractItem.src = this.sprInteractItem.url + "?" + new Date().getTime();
    this.sprBirth = sprites.player_birth;
    this.imgBirth = new Image();
    this.imgBirth.src = this.sprBirth.url + "?" + new Date().getTime();
    this.speed = 0;
    this.maxSpeed = 5;
    this.vector = new Vector(0, 0);
    this.distance = 0;
    this.willInteract = false;
    this.interEnc;
    this.interactCounter = 0;
    this.item;
    this.birthCounter = 0;
  }
  interact(encounter) {
    this.interEnc = encounter;
    if (this.state == "walking") {
      this.willInteract = true;
    } else if (this.state == "idle") {
      var direction = new Vector(encounter.x - this.x, encounter.y - this.y);
      this.distance = direction.length;
      if (this.distance <= 50) {
        this.state = "interacting";
        sound.play(sound.sounds.interact);
      }
    }
  }
  fight(enemy) {
    if (this.item !== undefined && this.item.counters.indexOf("fight") > -1) {
      enemy.life -= this.item.uses;
      if (enemy.life > 0) {
        this.gA.end();
      } else {
        if (enemy.life == 0) {
          this.item = undefined;
        }
        sound.play(sound.sounds[enemy.name]);
      }
    } else {
      this.gA.end()
    }
  }
  trap() {
    this.item.uses -= 1;
    if (this.item.uses == 0) {
      this.item = undefined;
    }
  }
  update() {
    switch (this.state) {
      case "pre":
        this.birthCounter = drawSpriteOnce(this.gA, this.imgBirth, this.sprBirth, this.x, this.y, this.birthCounter);
        if (this.birthCounter == this.sprBirth.anim) {
          this.state = "idle";
        }
        break
      case "walking":
        if (this.willInteract) {
          var direction = new Vector(this.interEnc.x - this.x, this.interEnc.y - this.y);
          this.distance = direction.length;
        } else {
          var direction = new Vector(this.gA.hoveredPos.x - this.x, this.gA.hoveredPos.y - this.y);
          this.distance = direction.length;
        }
        if (this.distance <= 10) {
          if (this.willInteract) {
            this.state = "interacting";
            sound.play(sound.sounds.interact);
            break;
          }
          this.state = "idle";
          drawSpriteLoop(this.gA, this.imgIdle, this.sprIdle, this.x, this.y);
        } else {
          direction.normalize();
          this.vector = this.vector.add(direction);
          this.vector.normalize();
          this.speed = this.speed + (this.maxSpeed - this.speed) / 6;
          this.x = this.x + this.speed * this.vector.x;
          this.y = this.y + this.speed * this.vector.y;
          // draw walking sprite
          drawSpriteLoop(this.gA, this.imgWalk, this.sprWalk, this.x, this.y);
          if (this.item !== undefined) {
            drawSpriteLoop(this.gA, this.item.img, this.item.spr, this.x, this.y);
          }
        }
        break;

      case "interacting":
        this.interactCounter = drawSpriteOnce(this.gA, this.imgInteractItem, this.sprInteractItem, this.x, this.y, this.interactCounter);
        if (this.interactCounter > 24) {
          this.interEnc.activate();
        }
        if (this.interactCounter == this.sprInteractItem.anim) {
          this.interactCounter = 0;
          this.willInteract = false;
          this.state = "walking";
        }
        break;
      default:
        drawSpriteLoop(this.gA, this.imgIdle, this.sprIdle, this.x, this.y);
        if (this.item !== undefined) {
          drawSpriteLoop(this.gA, this.item.img, this.item.spr, this.x, this.y);
        }
        break;
    }
  }

}

class Item {
  constructor(name, uses, value, counters) {
    this.name = name;
    this.uses = uses;
    this.value = value;
    this.counters = counters;
    this.spr = sprites["item_" + this.name];
    this.img = new Image();
    this.img.src = this.spr.url + "?" + new Date().getTime();
  }
}

var items = {
  stick: new Item('stick', 1, 1, ['fight']),
  paper: new Item('paper', 1, 1, ['hole']),
  stone: new Item('stone', 2, 1, ['fight', 'fight2']),
  coin: new Item('coin', 1, 1, []),
  flower: new Item('flower', 1, 1, ['girl'])
}

var encounterTypes = [
  "item",
  "fight",
  "fight2",
  "trade",
  "trap",
  "trade2",
  "trade3",
  "trap2"
]

var encounters = {
  item: [
    { name: "stick", reward: items.stick },
    { name: "stone", reward: items.stone }
  ],
  fight: [
    { name: "snake", life: 1, speed: 2 }
  ],
  fight2: [
    { name: "wolf", life: 2, speed: 3 },
  ],
  trade: [
    { name: "stick2paper", takes: "stick", gives: items.paper },
  ],
  trade2: [
    { name: "stone2coin", takes: "stone", gives: items.coin },
  ],
  trade3: [
    { name: "coin2flower", takes: "coin", gives: items.flower },
  ],
  trap: [{ name: "hole" }],
  trap2: [{name: "girl"}]
}

class Encounter {
  constructor(category, x, y, gA) {
    this.category = category;
    this.x = x;
    this.y = y;
    this.gA = gA;
    this.encounter = encounters[this.category][randInt(0, encounters[this.category].length - 1)];
    this.state = "hidden";
    this.sprHidden = sprites[this.encounter.name + "_hidden"];
    this.imgHidden = new Image();
    this.imgHidden.src = this.sprHidden.url + "?" + new Date().getTime();
    this.sprDepleted = sprites[this.encounter.name + "_depleted"];
    this.imgDepleted = new Image();
    this.imgDepleted.src = this.sprDepleted.url + "?" + new Date().getTime();
    if (this.category == "trap" || this.category == "trap2") {
      this.sprTrapped = sprites[this.encounter.name + "_trapped"];
      this.imgTrapped = new Image();
      this.imgTrapped.src = this.sprTrapped.url + "?" + new Date().getTime();
      this.sprOvercome = sprites[this.encounter.name + "_overcome"];
      this.imgOvercome = new Image();
      this.imgOvercome.src = this.sprOvercome.url + "?" + new Date().getTime();
      this.counterOvercome = 0;
      this.takeTrapItem = true;
    }
    this.counterDepleted = 0;
    this.counterTrapped = 0;
    this.numEnemies = 1;
    sound.play(sound.sounds.pop);
  }
  activate() {
    if (this.category == "trade" || this.category == "trade2" || this.category == "trade3") {
      if (this.gA.player.item !== undefined && this.gA.player.item.name == this.encounter.takes) {
        this.state = "depleted";
        this.gA.player.item = Object.assign({}, this.encounter.gives);
      }
    } else {
      this.state = "depleted";
      if (this.encounter.reward !== undefined && this.counterDepleted == this.sprDepleted.anim) {
        this.gA.player.item = Object.assign({}, this.encounter.reward);
      }
      if (this.category == "fight" || this.category == "fight2") {
        if (this.numEnemies > 0) {
          this.gA.enemies.push(
            new Enemy(this.gA, this.encounter.name, this.encounter.life, this.encounter.speed, this.x, this.y)
          )
          this.numEnemies -= 1;
        }
      }
    }
  }
  update() {
    switch (this.state) {
      case "depleted":
        this.counterDepleted = drawSpriteOnce(this.gA, this.imgDepleted, this.sprDepleted, this.x, this.y, this.counterDepleted);
        if (this.counterDepleted == this.sprDepleted.anim && (this.category == "trap" || this.category == "trap2")) {
          if (this.gA.player.item !== undefined && this.gA.player.item.counters.indexOf(this.encounter.name) > -1) {
            this.state = "overcome";
          } else {
            this.state = "trapped";
            if (this.category == "trap") {
              this.gA.player = undefined;
            }
          }
        }
        break;
      case "trapped":
        this.counterTrapped = drawSpriteOnce(this.gA, this.imgTrapped, this.sprTrapped, this.x, this.y, this.counterTrapped);
        if (this.counterTrapped == this.sprTrapped.anim) {
          this.gA.end();
        }
        break;
      case "overcome":
        this.counterOvercome = drawSpriteOnce(this.gA, this.imgOvercome, this.sprOvercome, this.x, this.y, this.counterOvercome);
        if (this.counterOvercome == this.sprOvercome.anim && this.takeTrapItem) {
          this.gA.player.trap();
          if (this.encounter.name == "girl") {
            this.gA.won = true;
            this.gA.end()
          }
          this.takeTrapItem = false;
        }
        break;
      default:
        drawSpriteLoop(this.gA, this.imgHidden, this.sprHidden, this.x, this.y);
        break;
    }
  }
}

class Enemy {
  constructor(gA, name, life, speed, x, y) {
    this.gA = gA
    this.name = name;
    this.life = life;
    this.maxSpeed = speed;
    this.speed = 0;
    this.x = x;
    this.y = y;
    this.sprIdle = sprites["enemy_" + this.name + "_idle"];
    this.imgIdle = new Image();
    this.imgIdle.src = this.sprIdle.url + "?" + new Date().getTime();
    this.sprRun = sprites["enemy_" + this.name + "_run"];
    this.imgRun = new Image();
    this.imgRun.src = this.sprRun.url + "?" + new Date().getTime();
    this.sprAttack = sprites["enemy_" + this.name + "_attack"];
    this.imgAttack = new Image();
    this.imgAttack.src = this.sprAttack.url + "?" + new Date().getTime();
    this.sprDead = sprites["enemy_" + this.name + "_dead"];
    this.imgDead = new Image();
    this.imgDead.src = this.sprDead.url + "?" + new Date().getTime();
    this.state = "wait";
    this.vector = new Vector(0, 0);
    this.wait = 75;
    this.attackCounter = 0;
    this.deadCounter = 0;
    sound.play(sound.sounds[this.name]);
  }
  update() {
    if (this.gA.player !== undefined) {
      var direction = new Vector(this.gA.player.x - this.x, this.gA.player.y - this.y);
      var distance = direction.length;
      if (this.state == "wait" && this.wait > 0) {
        this.wait -= 1;
      } else if (this.life <= 0) {
        this.state = "dead";
      } else if (distance > 50) {
        this.state = "run";
        this.attackCounter = 0;
      } else {
        this.state = "attack";
      }
    } else {
      this.state = "wait";
    }

    switch (this.state) {
      case "wait":
        drawSpriteLoop(this.gA, this.imgIdle, this.sprIdle, this.x, this.y)
        break;
      case "attack":
        this.attackCounter = drawSpriteOnce(this.gA, this.imgAttack, this.sprAttack, this.x, this.y, this.attackCounter);
        if (this.attackCounter == this.sprAttack.anim) {
          this.state = "wait";
          this.gA.player.fight(this);
        }
        break;
      case "dead":
        this.deadCounter = drawSpriteOnce(this.gA, this.imgDead, this.sprDead, this.x, this.y, this.deadCounter);
        break;
      default:
        // run
        direction.normalize();
        this.vector = this.vector.add(direction);
        this.vector.normalize();
        this.speed = this.speed + (this.maxSpeed - this.speed) / 6;
        this.x = this.x + this.speed * this.vector.x;
        this.y = this.y + this.speed * this.vector.y;
        drawSpriteLoop(this.gA, this.imgRun, this.sprRun, this.x, this.y)
        break;
    }
  }
}

class gameArea {
  constructor() {
    this.canvas = $("#gameArea")[0];
  }
  start() {
    console.log("game started");
    this.sprCursor = sprites.cursor;
    this.imgCursor = new Image();
    this.imgCursor.src = this.sprCursor.url + "?" + new Date().getTime();
    this.sprCursorClicked = sprites.cursor_clicked;
    this.imgCursorClicked = new Image();
    this.imgCursorClicked.src = this.sprCursorClicked.url + "?" + new Date().getTime();
    this.clicked = false;
    this.clickedCounter = 0;
    gameStarted = true;
    this.frameNo = 0;
    this.paused = false;
    this.canvas.width = 1024;
    this.canvas.height = 768;
    this.context = this.canvas.getContext("2d");
    this.interval = setInterval(this.updateGameArea.bind(this), 20);
    this.player;
    this.clickedPos;
    this.hoveredPos = { x: this.canvas.width / 2, y: this.canvas.height / 2 }
    this.encounters = [];
    this.encounterCounter = 0;
    this.enemies = [];
    this.level = 0;
    this.nextLevel = false;
    this.spawnEncounters = false;
    this.won = false;
    this.sprBoy = sprites.player_idle;
    this.imgBoy = new Image();
    this.imgBoy.src = this.sprBoy.url + "?" + new Date().getTime();
    this.sprGirl = sprites.girl_overcome;
    this.imgGirl = new Image();
    this.imgGirl.src = this.sprGirl.url + "?" + new Date().getTime();
    music.play(music.tracks.theme, true);
    this.delay = 125;
    
  }
  clearAll() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }
  end() {
    clearInterval(this.interval);
    setTimeout(function () {
      this.clearAll();
      this.context.beginPath();
      this.context.rect(0, 0, this.canvas.width, this.canvas.height);
      this.context.fillStyle = "#ddd";
      this.context.fill();
      music.pause(true);
  
      this.context.fillStyle = "#222";
      this.context.textAlign = "center";
      this.context.font = "30px Mansalva";
      if (this.won) {
        this.context.fillText("The end.", 512, 300);
        drawSpriteLoop(this, this.imgGirl, this.sprGirl, 400, 400);
        drawSpriteLoop(this, this.imgBoy, this.sprBoy, 600, 400);
      } else {
        this.context.fillText("You died.", 512, 300);
      }
    }.bind(this), 200);

  }

  updateGameArea() {
    if (!this.paused) {
      this.clearAll()
      for (let i = 0; i < this.enemies.length; i++) {
        const enem = this.enemies[i];
        enem.update();
      }
      var allDone = true;
      for (let i = 0; i < this.encounters.length; i++) {
        const enc = this.encounters[i];
        enc.update();
        this.delay = 125;
        if (!(enc.state == "depleted" || enc.state == "overcome") && !(enc.category == "trade" || enc.category == "trade2" || enc.category == "trade3")) {
          allDone = false;
          if (!(enc.state == "depleted" || enc.state == "overcome")) {
            this.delay = 500;
          }
        }
      }
      if (allDone && this.level != 0 && !this.nextLevel) {
        this.encounterCounter = 0;
        this.nextLevel = true;
      }
      if (this.clicked) {
        this.clickedCounter = drawSpriteOnce(this, this.imgCursorClicked, this.sprCursorClicked, this.hoveredPos.x, this.hoveredPos.y, this.clickedCounter);
        if (this.clickedCounter == this.sprCursorClicked.anim) {
          this.clicked = false;
          this.clickedCounter = 0;
        }
      } else {
        drawSpriteLoop(this, this.imgCursor, this.sprCursor, this.hoveredPos.x, this.hoveredPos.y);
      }
      if (this.player !== undefined) {
        this.player.update();
        if (this.encounterCounter > this.delay) {
          this.encounterCounter = 0;
          this.encounters = [];
          if (this.level == 0) {
            this.encounters.push(new Encounter("item", randInt(50, this.canvas.width - 50), randInt(50, this.canvas.height - 50), this));
            this.level = 1;
          } else {
            this.spawnEncounters = true;
          }
        } else if (this.nextLevel || this.level == 0) {
          this.encounterCounter += 1;
        }
      }
      if (this.spawnEncounters) {
        if (this.encounters.length > this.level) {
          this.spawnEncounters = false;
          this.level += 2;
          this.nextLevel = false;
        } else {
          if (everyInterval(10, this)) {
            if (this.encounters.length < 1) {
              this.encounters.push(new Encounter("item", randInt(50, this.canvas.width - 50), randInt(50, this.canvas.height - 50), this));
            } else if (this.encounters.length < 2) {
              var hasStone = false;
              for (let i = 0; i < this.encounters.length; i++) {
                const enc = this.encounters[i];
                if (enc.encounter.name == "stone") {
                  hasStone = true;
                }
              }
              if (hasStone || (this.player.item !== undefined && this.player.item.name == "stone")) {
                this.encounters.push(new Encounter(encounterTypes[randInt(0, 2)], randInt(50, this.canvas.width - 75), randInt(50, this.canvas.height - 75), this));
              } else {
                this.encounters.push(new Encounter(encounterTypes[randInt(0, 1)], randInt(50, this.canvas.width - 75), randInt(50, this.canvas.height - 75), this));
              }
            } else {
              var possibleChoices = [0]
              var hasPaperTrader = false;
              for (let i = 0; i < this.encounters.length; i++) {
                const enc = this.encounters[i];
                if (enc.encounter.name == "stick2paper") {
                  hasPaperTrader = true;
                }
              }
              var hasStick = false;
              for (let i = 0; i < this.encounters.length; i++) {
                const enc = this.encounters[i];
                if (enc.encounter.name == "stick") {
                  hasStick = true;
                }
              }
              var hasStone = false;
              for (let i = 0; i < this.encounters.length; i++) {
                const enc = this.encounters[i];
                if (enc.encounter.name == "stone") {
                  hasStone = true;
                }
              }
              var hasFlower = false;
              for (let i = 0; i < this.encounters.length; i++) {
                const enc = this.encounters[i];
                if (enc.encounter.name == "coin2flower") {
                  hasFlower = true;
                }
              }
              var hasCoin = false;
              for (let i = 0; i < this.encounters.length; i++) {
                const enc = this.encounters[i];
                if (enc.encounter.name == "stone2coin") {
                  hasCoin = true;
                }
              }
              if (hasStick || hasStone || (this.player.item !== undefined && this.player.item.name == "stone") || (this.player.item !== undefined && this.player.item.name == "stick")) {
                possibleChoices.push(1);
              }
              if (hasStone || (this.player.item !== undefined && this.player.item.name == "stone")) {
                possibleChoices.push(2);
                possibleChoices.push(5);
              }
              if (hasStick || (this.player.item !== undefined && this.player.item.name == "stick")) {
                possibleChoices.push(3);
              }
              if (hasPaperTrader || (this.player.item !== undefined && this.player.item.name == "paper")) {
                possibleChoices.push(4);
              }
              if (hasCoin || (this.player.item !== undefined && this.player.item.name == "coin")) {
                possibleChoices.push(6);
              }
              if (hasFlower || (this.player.item !== undefined && this.player.item.name == "flower")) {
                possibleChoices.push(7);
              }
              var choice = possibleChoices[randInt(0, possibleChoices.length - 1)];
              this.encounters.push(new Encounter(encounterTypes[choice], randInt(50, this.canvas.width - 75), randInt(50, this.canvas.height - 75), this));
            }
          }
        }
      }
      this.frameNo += 1;
    }
  }

  click(mousePos) {
    this.clickedPos = mousePos;
    this.hoveredPos = mousePos;
    if (this.player === undefined) {
      this.player = new Player(this);
      sound.play(sound.sounds.pop);
    } else if (this.player.state !== "interacting") {
      this.clicked = true;
      for (let i = 0; i < this.encounters.length; i++) {
        const enc = this.encounters[i];
        var direction = new Vector(mousePos.x - enc.x, mousePos.y - enc.y);
        var distance = direction.length;
        if (distance <= 37.5) {
          this.player.interact(enc);
        }
      }
    }
  }

  hover(mousePos) {
    this.hoveredPos = mousePos;
    if (this.player !== undefined) {
      if (this.player.state == "idle") {
        this.player.state = "walking";
      }
    }
  }
}

//Get Mouse Position
function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

